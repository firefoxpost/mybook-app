from flask import (
    Flask,
    jsonify,
    make_response,
    request,
    abort,
    Blueprint,
    render_template,
    redirect,
    g,
    url_for,
    Response,
    session
)
from requests import post

from jinja2 import TemplateNotFound


app = Flask(__name__)

from app import views
