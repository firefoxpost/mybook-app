from app import (
        app, jsonify, make_response,
        Blueprint, render_template, TemplateNotFound,
        redirect, abort, request)

from requests import post
import json

bp = Blueprint('mybook', __name__,
                url_prefix='/',
                template_folder='templates',
                static_folder='static'
              )

@bp.route('/')
def index():
    return get_html('auth.html')


@bp.route('/login', methods=['POST'])
def test():
    url = 'https://mybook.ru/api/auth/'
    res = post(url, data={
            "email": request.form['email'],
            "password": request.form['password']}
    )
    return res.headers.get('Set-Cookie')


@bp.route('/logout')
def logout():
    pass


app.register_blueprint(bp)


# вспомогательная функция для получения html-шаблонов
def get_html(file):
    try:
        return render_template(file)
    except TemplateNotFound:
        abort(404)
