(function(){
  $(document).ready(function(){
    $("#login_submit").on('click', function(){
      $.post( "/login", {
        email: $('#email').val(),
        password: $('#pwd').val()
      })
      .done(function( data ) {
        document.cookie = data
      });
    });
  });
})();
